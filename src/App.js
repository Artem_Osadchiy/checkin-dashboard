import React, { Component } from 'react';
import './App.css';

let users = [];

class App extends Component {
  
  state = {
    currentUser: null
  }
  
  componentDidMount() {
    Promise.resolve(require('./assets/data/mock-data.json'))
      .then(data => {
        users = data;
        this.setCurrentUser(users[0])
      })
  }
  
  render() {
    const {currentUser} = this.state;
    if(!currentUser) {
      return <div/>;
    }
    const cityTextColor = currentUser.testAlert ? '#009CDE' : 'gray';
    return (
      <div className="container">
        <div className="header-container" style={{...styles.blockContainer, ...{ padding: 10 }}}>
          <img src={require('./assets/Cross-Campus-logo.png')} className="logo-img" alt="logo"/>
          <b style={{fontSize: 18}}>Spinner</b>
        </div>
        <div className="content-container" >
          <div className="client-desc-container" >
            <img src={require('./assets/' + currentUser.imgSrc)} className="client-img" alt="User"/>
            <div className="client-desc-text-container" style={styles.flexColumnContainer}>
              {this.renderDescTextBlock(currentUser, cityTextColor, true)}
              {this.renderDescTextBlock(currentUser, cityTextColor, false)}
            </div>
            {currentUser.testAlert && <div className="client-desc-alert-line"/>}
          </div>
            <div className="alerts-info-container">
              {this.renderAlertsInfoBlock(currentUser, true)}
              {this.renderAlertsInfoBlock(currentUser, false)}
            </div>
            <div className="top-margin" style={styles.blockContainer}>
              <b className="alerts-info-text-title" style={{paddingRight: 40}}>Previous</b>
              {this.renderUsers()}  
            </div>
        </div>
      </div>
    );
  }
  
  renderDescTextBlock = (user, cityTextColor, top) => {
    const topText = top ? user.name : user.descField1;
    const topTextStyle = top ? {} : { marginBottom: 5 };
    const topTextClassName = top ? "client-desc-text-name" : "client-desc-text";
    const bottomText = top ? user.city : user.descField2;
    const bottomTextStyle = top ? { color: cityTextColor, marginTop: 5 } : { color: '#3F3F3F' };
    return (
      <div style={styles.flexColumnContainer}>
        <span className={topTextClassName} style={topTextStyle}>{topText}</span>
        <span className="client-desc-text" style={bottomTextStyle}>{bottomText}</span>
      </div>
    );
  };

  renderAlertsInfoBlock = (user, alerts) => {
    const title = alerts ? 'Alerts' : 'Info';
    const text = alerts ? user.testAlert : user.testInfo;
    return (
      <div className="alerts-info" style={styles.blockContainer}>
        <span className="alerts-info-text-title" style={{width: '30%'}}>{title}</span>
        <span className="alerts-info-text">{text}</span>
      </div>
    );
  };
  
  renderUsers = () => {
    const items = users.map(user => {
      const textColor = user.testAlert ? 'red' : 'black';
      const itemColor = this.state.currentUser.email === user.email ? '#E5E6E9' : 'white';
      return (
        <li key={user.email} style={{backgroundColor: itemColor}} onClick={() => this.setCurrentUser(user)}>
          <div style={{float: 'left', color: textColor}}>{user.name}</div>
          <div className="left-margin" style={{color: textColor}}>{user.email}</div>
        </li>
      );
    })
    return (
      <ul className="alerts-list">
        {items}  
      </ul>
    );
  };
  
  setCurrentUser = (currentUser) => this.setState({currentUser});
}

const styles = {
  blockContainer: {
    boxSizing: 'border-box', 
    padding: '1em', 
    backgroundColor: 'white'
  },
  flexColumnContainer: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between'
  }
}

export default App;

